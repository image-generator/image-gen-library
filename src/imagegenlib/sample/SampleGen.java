package imagegenlib.sample;

import imagegenlib.annotate.CanonConstructor;
import imagegenlib.annotate.GenClass;
import imagegenlib.generator.IGenerator;

@GenClass(
		name="Sample Generator",
		desc="A sample generator that returns a 1x1 black pixel"
)
public class SampleGen implements IGenerator{

	@CanonConstructor
	public SampleGen() {
		
	}
	
	@Override
	public int getColor(double x, double y) {
		return 0x00_00_00;
	}

	@Override
	public int getWidth() {
		return 1;
	}

	@Override
	public int getHeight() {
		return 1;
	}

}
