package imagegenlib.generator;

public interface IGenerator {
	
	
	int getColor(double x, double y);
	
	int getWidth();
	int getHeight();
}
