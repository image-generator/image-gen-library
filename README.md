# Image Gen Library

A library used for development with the Image Gen Desktop Application
The library is built on meta-programming, as opposed to a more structured framework in order to allow the coder complete and total liberty when designing their Image Gen algorithm.

To start developing an Image Gen algorithm, compile the class files into a .jar under JDK 11, and add it to your class path. To make a class registered to the Image Generator program, provide it with the necessary annotations as outlined in the SampleGen class provided.
